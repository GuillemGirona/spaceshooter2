﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Meteor : MonoBehaviour
{

    public bool launched;
    protected Vector2 moveSpeed;
    protected float rotationSpeed;
    public ParticleSystem explosion;
    public GameObject graphics;

    private Collider2D meteorCollider;



    private Vector3 iniPos;

    void Start()
    {
        iniPos = transform.position;
        meteorCollider = GetComponent<Collider2D>();
    }

    public void LaunchMeteor(Vector3 position, Vector2 direction, float rotation)
    {
        transform.position = position;
        launched = true;
        transform.rotation = Quaternion.Euler(0, 0, 0);
        moveSpeed = direction;
    }

    // Update is called once per frame
    protected void Update()
    {
        if(launched)
        {
            transform.Translate(moveSpeed.x * Time.deltaTime, moveSpeed.y * Time.deltaTime, 0);
            transform.Rotate(0, 0, rotationSpeed * Time.deltaTime);
        }
    }

    protected void OnTriggerEnter2D(Collider2D other)
    {
        if(other.tag == "Bullet")
        {
            Explode();
        }
        else if(other.tag == "Finish")
        {
            Reset();
        }
    }

    protected void Reset()
    {
        transform.position = iniPos;
        launched = false;
        graphics.SetActive(true);
        meteorCollider.enabled = true;
    }

    protected virtual void Explode()
    {
        launched = false;
        graphics.SetActive(false);
        meteorCollider.enabled = false;
        explosion.Play();
        Invoke("Reset", 1);
    }
}